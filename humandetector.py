import os

class HumanDetector:
    def __init__(self):
        self.stream = os.environ['STREAM']
        self.api = os.environ['TG_API']
        self.chat_id = os.environ['TG_CHAT_ID']
        self.cam_name = os.environ['CAMERA']

    def env_vars(self):
        return(self.stream, self.api, self.chat_id, self.cam_name)
