import telebot 

class TelegramAlertotron:
    def __init__(self, api_token, chat_id, subject, body):
        self.api_token = api_token
        self.bot = telebot.TeleBot(self.api_token)
        self.bot.config['api_key'] = self.api_token
        self.chat_id = chat_id
        self.subject = subject
        self.body = body

    def send_alert(self):
        return(self.bot.send_message(self.chat_id, self.body))

    def send_photo(self, image):
        raise(NotImplementedError)
        #return(self.bot.send_photo(self.chat_id, image))
