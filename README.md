A project to take video input, detect humans and send Telegram alerts.
I'll create a ContainerFile for the project once it's finished and 
provide instructions for running the container with Podman.

## How to use:
This project reads from Environment variables to ensure compatibilty with
Kubernetes deployments. The accepted and required environment varaibles are:

~~~
STREAM=http://username:password@192.168.1.243/video.cgi
TG_API=TELEGRAM_API_KEY
TG_CHAT_ID=-TELEGRAM_CHAT_ID
CAMERA=NAME_OF_CAMERA
~~~

#### STREAM
This can be a rtsp or http stream. The credentials are basic-auth
and provided in the URI

#### TG_API
This is your Telegram API key. See the following link for more details:
https://core.telegram.org

#### TG_CHAT_ID
This is the ID that uniquely identifies the Chat where messages will be
sent

#### CAMERA
The name of the camera. This appears in the Telegram messages, eg:
"A hooman was detected by the Office camera"

# Container

To use the container:
~~~
podman run -n humandetector -d \
-e STREAM=http://username:password@192.168.1.243/video.cgi \
-e TG_API=TELEGRAM_API_KEY \
-e TG_CHAT_ID=-TELEGRAM_CHAT_ID \
-e CAMERA=NAME_OF_CAMERA \
registry.gitlab.com/shep03/humandetector:v1.0.0
~~~

# Kubernetes
