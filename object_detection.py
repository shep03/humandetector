# object detection webcam example using tiny yolo
# usage: python3 object_detection.py

from humandetector import HumanDetector
from telegram.telegramalertotron import TelegramAlertotron

import cvlib as cv
from cvlib.object_detection import draw_bbox
import cv2
from datetime import datetime, timedelta

# We'll use OS Environment variables to make this work better
# in a Kubernetes / Container environment.
hd = HumanDetector()
stream, api, chat_id, cam_name = hd.env_vars()

fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
time_sent = datetime.now()

# open stream
_camera = cv2.VideoCapture(stream)

if not _camera.isOpened():
    print(f"Could not open {cam_name}")
    exit()


def telegram_notify(message):
    bot = TelegramAlertotron(api, chat_id, "Alert", message)
    return(bot.send_alert())


def detect_and_notify(label, conf, time, out):
    print(f"{label} detected with {conf}")
    message = f"Person detected on {cam_name} camera!"
    next_send = time + timedelta(minutes=2)

    # We will eventually use these files to send via telegram.
    # For now, we'll just write them to the disk:
    cv2.imwrite(f"{cam_name}_{time}_detection.jpeg", out)

    if datetime.now() > next_send:
        telegram_notify(message)


def main():
    time = datetime.now()
    try:
        # loop through frames
        while _camera.isOpened():

            # read frame from webcam
            status, frame = _camera.read()

            if not status:
                break

            # apply object detection
            bbox, label, conf = cv.detect_common_objects(
                frame, confidence=0.40, model='yolov4-tiny')

            # draw bounding box over detected objects
            out = draw_bbox(frame, bbox, label, conf, write_conf=True)

            # For debugging, this will display the output
            # cv2.imshow("Real-time object detection", out)

            # Only take action if there is a person captured.
            if 'person' in label:
                detect_and_notify(label, conf, time, out)
                time = datetime.now()
            
            # press "Q" to stop
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    except Exception:
        raise
    finally:
        # release resources
        _camera.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
